#!/bin/bash

echo starting mynet images

docker network rm mynet
docker network create --subnet=10.0.0.0/16 mynet
docker run -p 4000:8080 --net=mynet --ip=10.0.0.20 -d hw2
docker run -p 4001:8080 -e MAINIP=10.0.0.20:8080 -d hw2
docker run -p 4002:8080 -e MAINIP=10.0.0.20:8080 -d hw2