'''
1. building the dictionary for ip hashes
    -check that every node will perform with direct requests

2. given a key figureout which host should receive a request
    - input: {'10.0.0.20:8080':'hashed key'}
    - output: ip of host
    - sort by                   ^^^^^^^^
    - for loop that finds sucsesssereror

2.5: put request to specific node and store it


3. implement delete node (distributing the local keys)
    -input: node to delete
    -output: send put requests to the next node

4. implement add new node (split keys up)
    -input: node id , hash
    -figure out where this belongs in the ring
    -contact the successor node
    -successor node needs to delete and put its own

'''



from flask import Flask, request, jsonify

import requests
import os
import operator

app = Flask(__name__)

#getting the environment variables
IP_ENV = os.environ.get('IP')      #.get() is a safe way to get values from a dictionary if their key might not exist
VIEW = os.environ.get('VIEW')
ip_port = os.environ.get('ip_port')
hostlist = str(VIEW).split(',')
#hash the local host ip
local_host_ip_hash = hash(str(IP_ENV))


#-----------------feature 1-------------------------------------#
hostDictionary = {}
testHostlist = ['10.0.0.20:8080','10.0.0.21:8080','10.0.0.22:8080','10.0.0.23:8080','10.0.0.24:8080']



def make_ip_hash(hostlist):
    for host in hostlist:
        hostDictionary[host] = abs(hash(str(host))) 
#-----------------feature 1-------------------------------------#

#-----------------feature 2-------------------------------------#
testDictionary = {'10.0.0.20:8080':abs(hash('10.0.0.20:8080')),
                  '10.0.0.21:8080':abs(hash('10.0.0.21:8080')),
                  '10.0.0.22:8080':abs(hash('10.0.0.22:8080')),
                  '10.0.0.23:8080':abs(hash('10.0.0.23:8080')),
                  '10.0.0.24:8080':abs(hash('10.0.0.24:8080')),}

sortedDictionary = []

def sortedDict(dictionaryThing):
     sortedDictionary=sorted(dictionaryThing.items(), key=operator.itemgetter(1))
     return sortedDictionary

#at this point we can assume the dictionary is sorted

def findSuccessor(key, dictionaryThing):
    sortedDictionary=sortedDict(dictionaryThing)
    for ip, hashedIP in sortedDictionary: #want to go through dictionary values
        print 'loop iteration ' 

        if abs(hash(str(key))) < hashedIP:   #comparing hash of the key's string to hashedIP
            print 'returned ip: '+ str(ip) + ", whose hash is " + str(hashedIP)
            return ip
    print 'defaulted: '+str(sortedDictionary[0])
    return sortedDictionary[0] #return the key of the first element in the sorted dictionary

def testValue(testKey):
    print "___________________________________________________________________________"
    hashedKey=abs(hash(str(testKey))) #hashes the STRING of the key    
    print "Our key is " + str(testKey) + ", whose hash is " + str(hashedKey)
    print "The successor of this key is " + str(findSuccessor(testKey,testDictionary))


if __name__ == "__main__":
    print 'This is the original unsorted dictionary'
    print testDictionary
    print 'This is the sorted dictionary (which is actually a list)'
    print sortedDict(testDictionary)

    # print 'Our key is 69'
    # print 'Our key hashed is ' + str(abs(hash('69')))
    # print 'The successor of this key is ' + str(findSuccessor(69, testDictionary)) 
    
    testValue(69) 
    testValue("10.0.0.23:8080") #can't take it in because of second period
    testValue("10.0.0.22:8080")

    print testHostlist
    make_ip_hash(testHostlist)
    print hostDictionary


    # print 'Our key hashed is ' + str(abs(hash('asdfjkl;')))
    # print 'The successor of this key is ' + str(findSuccessor('asdfjkl;', testDictionary))
    # print 'Our key is 10.0.0.24:8080'
    # print 'Our key hashed is ' + str(abs(hash('10.0.0.24:8080')))
    # print 'The successor of this key is ' + str(findSuccessor('10.0.0.24:8080', testDictionary))
# 4223928106435195318 - key

#  682632314363838447 - first one

# 682632314363838447
# 896815035514674887
# 2257309893720724682
# 2891211733966435624
# 4223928106435195317





   