from flask import Flask, request, make_response, jsonify

import requests
import os
import sys
import operator
import hashlib

app = Flask(__name__)

#getting the environment variables
IP_ENV = os.environ.get('ip')      #.get() is a safe way to get values from a dictionary if their key might not exist
VIEW = os.environ.get('VIEW')
ip_port = os.environ.get('ip_port')
hostlist = []
forwarded = False



if VIEW is None:
    hostlist.append(ip_port)
else:
    hostlist = str(VIEW).split(',')

def hashIt(inputHash):
    #return(abs(int(hashlib.sha224(inputHash).hexdigest(),16)))
    # return abs(hash(str(hash(str(inputHash)))))
    return abs(hash(str(inputHash)))


#hash the local host ip
local_host_ip_hash = hashIt(str(IP_ENV))
lookup = {}


hostDictionary = dict((host,abs(hashIt(str(host)))) for host in hostlist)

'''builds ip:hash(ip) dictionary'''
def make_ip_hash(hostlist):
    for host in hostlist:
        hostDictionary.update({str(host):hashIt(str(host))})        


'''returns sorted list of tuples by hash(ip) values from host dictionary'''
def sortedDict():
    global hostDictionary
    sortedDictionary=sorted(hostDictionary.items(), key=operator.itemgetter(1))
    print 'hostDictionary sorted is :' + str(sortedDictionary)
    return sortedDictionary

'''returns desired host of given key'''
def findSuccessor(key, dictionaryThing):
    sortedDictionary=sortedDict()
    for ip, hashedIP in sortedDictionary: #want to go through dictionary values
        print 'loop iteration ' 
        if hashIt(str(key)) < hashedIP:   #comparing hash of the key's string to hashedIP
            print 'returned ip: '+ str(ip) + ", whose hash is " + str(hashedIP)
            return str(ip)
    print 'defaulted: '+str(sortedDictionary[0])
    return str(sortedDictionary[0][0]) #return the key of the first element in the sorted dictionary

'''test function: prints key and hash(key) and successor'''
def testValue(testKey):
    print "___________________________________________________________________________"
    hashedKey=hashIt(str(testKey)) #hashes the STRING of the key    
    print "Our key is " + str(testKey) + ", whose hash is " + str(hashedKey)
    print "The successor of this key is " + str(findSuccessor(testKey,hostDictionary))

'''key-value GET, PUT, and DELETE Requests'''
@app.route('/kvs', methods=['GET', 'PUT','DELETE'])
def stuff():
    global lookup
    global hostDictionary
    global hostList
    if request.method == 'PUT':
        key = request.form.get('key') 
        value = request.form.get('value')

        if key == None or value == None:
            j = jsonify(replaced=0, msg='fail', owner= keyOwner, test="key or value undefined")
            return (make_response(j,200,{'Content-Type':'application/json'}))

        keyOwner = findSuccessor(key, hostDictionary)
        if keyOwner == ip_port:
            alreadyExists = key in lookup; 
            lookup[key] = value
            if(alreadyExists):  
                j = jsonify(replaced=1, msg='success', owner= keyOwner)
                return (make_response(j,200,{'Content-Type':'application/json'}))
            else:
                j = jsonify(replaced=0, msg='success', owner= keyOwner)
                return (make_response(j,200,{'Content-Type':'application/json'}))                
        else:         
            key = str(request.form.get('key'))  #TODO: what is 2nd arg
            value = str(request.form.get('value'))
            r = requests.put('http://'+str(keyOwner)+'/kvs', data = request.form)
            return r.text,r.status_code

        if request.method == 'GET':
            key = request.form.get('key') 
            keyOwner = findSuccessor(key, hostDictionary)

            if keyOwner == ip_port:
                if(key):
                    if( key in lookup):
                        j = jsonify(msg= 'success', value= value, owner= keyOwner)
                        return (make_response(j,200,{'Content-Type':'application/json'}))
                    else:
                        j = jsonify(msg='error',error='key does not exist')
                        return make_response(j,404,{'Content-Type':'application/json'})
                else:
                    j = jsonify(msg='error',error='key does not exist')
                    return make_response(j,404,{'Content-Type':'application/json'})
            else:
                if request.method == 'GET':
                    key = request.args.get('key')
                    r = requests.get('http://'+str(keyOwner)+'/kvs?key='+str(key))
                    return r.text,r.status_code

'''Returns number of kvs in node'''
@app.route('/kvs/get_number_of_keys', methods=['GET'])
def numKeys():
    return jsonify(count=len(lookup)), 200

'''RECEIVE HOST LIST UPDATE TO REMOVE OR ADD A NODE TO LOCAL 'VIEW'''
@app.route('/kvs/update_hostlist', methods=['PUT'])
def hostList_update():
    if request.method == 'PUT':

        request_ipPort = request.form.get('input_host')
        request_type = request.form.get('request_type')
        global hostDictionary

        if(request_type == 'add'):
            # add new host to local local directory
            if (request_ipPort not in hostlist):
                hostlist.append(str(request_ipPort))
                hostDictionary = dict((host,abs(hash(host))) for host in hostlist)
                #hostDictionary = make_ip_hash(hostlist)
                
        elif(request_type == 'remove'):
            #removes new host from local directory
            if(request_ipPort in hostlist):
                hostlist.remove(str(request_ipPort))
                hostDictionary = dict((host,abs(hash(host))) for host in hostlist)

        j = jsonify(msg='success',hostDictionary=str(list(hostDictionary.keys())),hostList=str(hostlist),lastHost=request_ipPort,request_type=request_type)
        return (make_response(j,200,{'Content-Type':'application/json'}))

#adding/deleting nodes -------------------------------
@app.route('/kvs/view_update', methods=['PUT'])
def viewUpdate():
    global hostDictionary
    if request.method == 'PUT':   

        # parse request form
        request_ipPort = request.form.get('ip_port') 
        request_type = request.form.get('type')
        forwardedIP_port = request.form.get('forwardedIP_port')
        

        if request_type == 'add':
            # check if new ip is in hostDictionary   
            
            #Determine who is the successor of the new node.
            successor = findSuccessor(request_ipPort, hostDictionary)


            if successor == ip_port: #We are the successor of the incoming node
                #add node to local hostDictionary
                hostlist.append(str(request_ipPort))
                hostDictionary[str(request_ipPort)] = hashIt(str(request_ipPort))

                #update the new node's hostlist with existing IP's
                for host in list(hostDictionary.keys()):
                    #put request to new node
                    if host == request_ipPort:
                        continue
                    r = requests.put('http://'+str(request_ipPort)+'/kvs/update_hostlist',data = {'input_host':str(host),'request_type':'add'})

                # send all keys that belong to the new node to the new node
                i = 0
                for key in lookup.keys():
                    if (hashIt(str(key)) < hashIt(str(request_ipPort))): #does key belong at new node?
                        i = i + 1
                        #add key to new node, delete key from old node
                        r = requests.put('http://'+str(request_ipPort)+'/kvs', data = {'key':str(key),'value':lookup[key]})
                        lookup.pop(key, None) 
                        if(i > 100):
                            break

                #broadcast: send put request with new node info to all nodes EXCEPT new node, forwarding node, and successor
                for host in list(hostDictionary.keys()):            
                    if ((host == request_ipPort) or (host == ip_port) or (host == forwardedIP_port)):      
                        pass
                    else:
                        r = requests.put('http://'+str(host)+'/kvs/update_hostlist',data = {'input_host':str(request_ipPort),'request_type':'add'})

    
                ##########wait for confirmation###########

                j = jsonify(msg= 'success', text=str(r.text),statusCode=str(r.status_code))
                return (make_response(j,200,{'Content-Type':'application/json'}))

            else:   #not successor node (forward request to successor)
                r = requests.put('http://'+str(successor)+'/kvs/view_update', data={'ip_port':str(request_ipPort), 'type':'add', 'forwardedIP_port':str(ip_port)})
            
                hostlist.append(str(request_ipPort))
                hostDictionary[str(request_ipPort)] = hashIt(str(request_ipPort))
                j = jsonify(msg='success', hostDict= str(hostDictionary.keys()))
                return (make_response(j,200,{'Content-Type':'application/json'}))

        elif request_type == 'remove':
            #check if we are the node to be deleted
            if request_ipPort == ip_port:
                hostDictionary.pop(str(ip_port))
                
                # j = jsonify(msg="success", ip_port=ip_port)
                # return (make_response(j,200,{'Content-Type':'application/json'}))
                
                successor = findSuccessor(ip_port, hostDictionary)

                j = jsonify(msg="success", ip_port=ip_port)
                return (make_response(j,200,{'Content-Type':'application/json'}))

                for key in lookup.keys():
                    r = requests.put('http://'+str(successor)+'/kvs', data ={'key':str(key), 'value':str(lookup[key])})

                j = jsonify(msg="success", ip_port=ip_port)
                return (make_response(j,200,{'Content-Type':'application/json'}))
            else:
                #forward to node to be deleted
                r = requests.put('http://'+request_ipPort+'/kvs/view_update', data={'ip_port':str(request_ipPort), 'type':'remove', 'forwardedIP_port':str(ip_port)})
                hostDictionary.pop(str(request_ipPort),None)
                hostlist.remove(str(request_ipPort))

                #tell everyone to remove node from hostlist
                for host in list(hostDictionary.keys()):
                    if (host==ip_port):
                        continue
                    r = requests.put('http://'+str(host)+'/kvs/update_hostlist',data = {'input_host':str(request_ipPort),'request_type':'remove'})

                j = jsonify(msg='success', json=r.json())
                return (make_response(j,200,{'Content-Type':'application/json'}))     

#debugging code --------------------------------------
@app.route('/throwup', methods=['GET'])
def throwup():
    if request.method == 'GET': 
        return jsonify(count=len(lookup),hostlist=hostlist, successor= findSuccessor(ip_port, hostDictionary), lookup=lookup, hash=hashIt(ip_port), ip_port=str(ip_port), hostDictionary=hostDictionary)
#----------------------------------------------------


if __name__ == "__main__":
    #app.run(host=IP_ENV, port=PORT_ENV)
    app.run("0.0.0.0", port=8080)
    # print 'started node'
    # print 'hostlist: ' +str(hostlist)
    # print 'hostDictionary: ' + str(hostDictionary)
    # testValue('10.0.0.4:8080') 


   