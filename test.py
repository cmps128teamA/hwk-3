import requests
import random
import string

min_char = 8
max_char = 12
allchar = string.ascii_letters + string.punctuation + string.digits

addrStr = "http://server-hostname:8080/kvs"

def getRandStr():
	password = "".join(random.choice(allchar) for x in range(random.randint(min_char, max_char)))
	return password

'''Makes random put requests to random hosts within a given port range'''
if __name__ == "__main__":
	start_port = 40001 #start of range
	stop_port = 40010 #end of range

	num_instances = start_port - stop_port + 1

	dictList = [dict() for x in range(start_port, stop_port+1)] #instance dictionary

        for i in range(20):
			key = getRandStr()
			value = random.randint(1,101)
			instance = random.randint(start_port, stop_port+1) #picks a port (host)
			dictList[instance][key] = value #add kv to global dictionary
			payload = {"key": key, "value": value}
			s = addrStr+":"+str(instance) #makes servername+port url
			r = requests.put(s, data=payload) #put request
